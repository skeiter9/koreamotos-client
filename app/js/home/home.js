'use strict';

angular.module('app')

  .config( function( $stateProvider ) {

  	$stateProvider

  	  .state('home',{
          url:'/home',
          resolve: { 
            usuarioData: function(rest){  
              return rest('usuario/data','POST',{ token: localStorage.getItem('token') });
            }
          },
          templateUrl:'js/home/home.html',
          controller: function($rootScope, $scope, usuarioData){
            
            $rootScope.$emit('changeTypeToolbar','appbar', 'normal');
            $rootScope.$emit('changeTitleToolbar', 'appbar','Home');
            $rootScope.$emit('hideAction');
            console.log(usuarioData);
            var vm = this;
            vm.user = usuarioData.respond.usuario.respond[0];
            $rootScope.app.menu = usuarioData.respond.asignacion_menu.respond;

          },
          controllerAs: 'vm'
      });

  });
