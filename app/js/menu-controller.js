'use strict';

angular.module('app')
	.controller('menu', function($scope, $state, $rootScope){

		$scope.goToState = function( state ) {
			$state.go(state);
			$rootScope.$emit('toggleSidenav', 'left', false);
		}

	});
