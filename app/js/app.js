'use strict';

angular.module('app',['yemd', 'ui.router', 'angularFileUpload']);

angular.module('app')

	.config(function($yemdProvider, $stateProvider, $urlRouterProvider) {

		$urlRouterProvider 
	    .rule(function ($injector, $location) {
	      var path = $location.path(),
	          normalized = path.toLowerCase();
	      if (path !== normalized) {
	        return normalized;
	      }
	  	}).otherwise('/');

	  $stateProvider
			.state('404',{
	      url:'/404',
	      templateUrl:'js/components/views/404.html',
	      controller: ['$scope','$rootScope', '$state',function($scope,$rootScope, $state){ 
	        
	        var vm = this; 
	        $rootScope.$emit('changeTitleToolbar', 'appbar','404 Error');
	        $rootScope.$emit('changeTypeToolbar','appbar', 'default');
	        $rootScope.$emit('hideAction');

	      }],
	      controllerAs:'vm'
	  	});

	})

	.run(function($state, $urlRouter,$rootScope,$rootElement, rest, $yemd, $verge){

		$rootScope.app = {
			logged: false,
			logout : function(){
				localStorage.removeItem('token');
				$rootScope.$emit('toggleSidenav', 'left', false);
        $rootScope.app.logged = false;
        $state.go('login');//
			},
			toolbarIcon : {
				cancel: { show: false, click: function(){} },
				edit: { show: false, click: function(){} },
				delete: { show: false, click: function(){} },
			},
			openMenu: function( sidenavType ) {
				$rootScope.$emit('toggleSidenav', sidenavType, true);
			}
		};

		//$rootScope.serverUrl = 'http://koreamotos.yetsu.com' ; //production
		$rootScope.serverUrl = 'http://koreamotos.com.pe' ; //dev
		$rootScope.app.title = " Korea Motos ";

		//------- State Events

	  //Check Auth
		$rootScope.$on('$locationChangeSuccess', function(evt) { 
			
	    evt.preventDefault(); 

	    if ( localStorage.getItem('token') !== null  ) { $rootScope.app.logged = true; } 
	    else if ($state.current.name !== 'login') {  return $state.go('login') ; }

	  });

		//Not found
		$rootScope.$on('$stateNotFound', function(event, unfoundState, fromState, fromParams){ 
	    $state.go('404',{stateFail:unfoundState.to});
	  });

	});
