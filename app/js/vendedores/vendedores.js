'use strict';

angular.module('app')
  .config(function( $stateProvider ) {

    var model = 'trabajador',
        modelUri = 'vendedores',
        listTemplate = {
          templateUrl: "js/vendedores/vendedores-list.html" ,
          controller: vendedoresListController
        },
        modelNew = {
            templateUrl: "js/vendedores/vendedores-new.html" ,
            controller: vendedoresNewController
        },
        modelShow = {
          templateUrl: "js/vendedores/vendedores-show.html",
          controller: vendedoresShowController
        };


	  $stateProvider

      .state( model,{
          abstract: true, 
          url:'/'+modelUri
        })  

      .state(model+'.list',{
        url:'',
        resolve: { 
          module: function(rest){  
            return rest( model+'/vendedores' ,'POST',{ token: localStorage.getItem('token') });
          }
        }, 
        views: {
          '@': listTemplate
        }
            
      })

      .state(model+'.show',{ 
        url: '/{id:[0-9]{1,25}}', 
        resolve: {
          module: function(rest){  
            return rest( model+'/vendedores' ,'POST',{ token: localStorage.getItem('token') });
          }
        }, 
        views: {
          '@': listTemplate,
          'sidenavRight@': modelShow
        }
      })

      .state( model +'.new',{
        url:'/new', 
        resolve: { 
          module: function(rest){  
            return rest( model+'/vendedores' ,'POST',{ token: localStorage.getItem('token') });
          },
          //form:['rest',function(rest){  
            //return rest(model + '/new','GET',{});
          //}],
          areas:['rest',function(rest){  
            return rest('area','POST', { token: localStorage.getItem('token') });
          }],
          cargos:['rest',function(rest){  
            return rest('cargo','POST', { token: localStorage.getItem('token') });
          }]
        }, 
        views: {
          '@': listTemplate ,
          'sidenavRight@': modelNew
        }   
      }) ;

  });

function vendedoresShowController (module, findByAttribute, $verge, $scope, $stateParams, $rootScope, $state){

  $rootScope.app.toolbarIcon.cancel.show = true;
  $rootScope.app.toolbarIcon.cancel.click = function() {
    $state.go('^.list');
    $rootScope.$emit('toggleSidenav', 'right', false);
  };
  $rootScope.app.toolbarIcon.edit.show = true;
  $rootScope.app.toolbarIcon.edit.click = function() {
    $state.go('^.edit');
  };
  $rootScope.app.toolbarIcon.delete.show = true;
  $rootScope.app.toolbarIcon.delete.click = function() {
    $state.go('^.list');
  };


  $rootScope.$emit('changeTypeToolbar','toolbar-right', 'extend');
  
  $rootScope.$emit('toggleSidenav', 'right', true);

  $rootScope.$on('clickOverlay', function(){
    $state.go('^.list');
  });

  if ( $('.sidenav--right').find('.sidenav__content').height() > $verge.viewportH() ) {
    $('.sidenav--right').css('overflow-y', 'scroll');
  };

  $scope.item = findByAttribute( module.respond['trabajador'].respond, 'id_trabajador', $stateParams.id )[0];
  $rootScope.$emit('changeTitleToolbar', 'toolbar-right', $scope.item.apellido_paterno +' '+$scope.item.apellido_materno +' '+$scope.item.nombres);

  var photo = $rootScope.serverUrl+'/public/persona/'+ $scope.item.photo;//$scope.item.photo.split('.')[0]+"-4x3."+$scope.item.photo.split('.')[1] ;
  $('#vendedor-photo').css( 'background-image', "url('"+ photo +"')" );

}

function vendedoresListController ($scope, $rootScope, $yemd, $state, module, list, $verge){ 

  $rootScope.$emit('changeTypeToolbar', 'appbar','extend');
  $rootScope.$emit('changeTitleToolbar', 'appbar','Vendedores');

  $rootScope.$emit('showAction',{ icon: 'mdfi_content_add', type: 'embed', nodeClose: '#appbar', classSpecial: 'action--in-appbar' });
  
  $rootScope.$on('clickAction',function(e){ $state.go('^.new');  });
  console.log(module);
  $rootScope.app.user = module.respond.usuario.respond[0];
  $rootScope.app.menu = module.respond.asignacion_menu.respond;

  if ( $('.sidenav--right').find('.sidenav__content').height() > $verge.viewportH() ) {
    $('.sidenav--right').css('overflow-y', 'scroll');
  };

  var data = [ 
    { id_trabajador : 'id' }, 
    { apellido_paterno : 'apellido paterno' }, 
    { apellido_materno : 'apellido materno' }, 
    { nombres : 'nombres' }, 
    { photo : 'photo' /*, value : function (value) { return  value.split('.')[0]+"-1x1."+value.split('.')[1]  }*/  }, 
    { dni : 'dni' }, 
    { area : 'área' }, 
    { cargo : 'cargo' }, 
    { estado : 'estado del trabajador' , value : function (value) { return ( value==='1') ? 'Activo' : 'No ctivo'  }  } 
  ] ;

  $scope.items = list(module.respond['trabajador'].respond, data);
  
  $scope.openItem = function(item,event){
    console.log(item);
    $state.go('^.show', { id:item })
  }

}

function  vendedoresNewController ($scope, findByAttribute, $verge, $rootScope, $yemd, areas, cargos , rest, validForm, $state, $upload ){  
  
  $rootScope.$emit('changeTitleToolbar', 'toolbar-right','Registrar Vendedor');

  $rootScope.app.toolbarIcon.cancel.show = true;

  $rootScope.app.toolbarIcon.cancel.click = function() {
    $state.go('^.list');
    $rootScope.$emit('toggleSidenav', 'right', false);
  };

  $rootScope.app.toolbarIcon.edit.show = false;
  $rootScope.app.toolbarIcon.delete.show = false;

  $rootScope.$emit('toggleSidenav', 'right', true);

  $rootScope.$on('clickOverlay', function(){
    $state.go('^.list');
  });

  //if ( $('.sidenav--right').find('.sidenav__content').height() > $verge.viewportH() ) {
    $('.sidenav--right').css('overflow-y', 'scroll');
  //};

  $scope.formApp = {
    asignacion_cargo : {
      id_cargo: 4 ,
      fecha_inicio: new Date,
      fecha_fin: '01-12-2015',
    },
    persona : {},
    noplanilla: {},
    trabajador: {
      estado: 1,
      fecha_contratacion: new Date
    },
    usuario: {

    },
    pago: {
      id_tipo: 2
    }
  }; 

  //Models involved
  $scope.areas = areas.respond.area.respond;

  function submit (form){

    var validFormResult = validForm(form) ;  

    $scope.formApp.noplanilla.sueldo = 1000;

    if ( validFormResult.status ) {
      $scope.formApp.token = localStorage.getItem('token') ;
      $scope.upload = $upload.upload({
          url: $rootScope.serverUrl+'/rest/trabajador/new', //upload.php script, node.js route, or servlet url
          method: 'POST',
          file: $scope.photo,
          headers: {
            'Content-Type':  $scope.photo.type
          },
          data: {token: localStorage.getItem('token') , form: $scope.formApp }
        }).progress(function(evt) {
          console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
        }).success(function(data, status, headers, config) {
                      
          console.log(data, status);

        }).error(function(error){
                      console.log(error);
        }); 

    }else{
                 
      $rootScope.$emit('showSnackbar', validFormResult.message, form ) ;

    }; 

  };

  

  $scope.formActions = {
    cancel : function() { $rootScope.$emit('toggleSidenav', 'right', false); $state.go('^.list'); },
    submit : submit 
  }

  $scope.previewFile = function () {
    var preview = document.querySelector('img');
    var file    = document.querySelector('input[type=file]').files[0];
    var reader  = new FileReader();

    reader.onloadend = function () {
      preview.src = reader.result;
    }

    if (file) {
      reader.readAsDataURL(file);
    } else {
      preview.src = "";
    }
  };

  $scope.onFileSelect = function($files) {
    for (var i = 0; i < $files.length; i++) {
      $scope.photo = $files[i];

    }
  };


}

