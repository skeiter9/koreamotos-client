'use strict';

angular.module('app')
  
  .config(function( $stateProvider ) {

    var model = 'evaluacion',
        modelUri = 'evaluacion',
        listTemplate = {
          templateUrl: "js/evaluacion/evaluacion.html" ,
          controller: evaluacionListController
        },
        modelNew = {
          templateUrl: "js/"+modelUri+'/'+modelUri+"-new.html" ,
          controller: evaluacionNewController
        },
        modelShow = {
          templateUrl: "js/"+modelUri+'/'+modelUri+"-show.html",
          controller: evaluacionShowController
        };

    $stateProvider

      .state( model,{
          abstract: true, 
          url:'/'+modelUri
        })  

      .state(model+'.list',{
        url:'',
        resolve: { 
          module: function(rest){  
            return rest( 'trabajador/vendedores' ,'POST',{ token: localStorage.getItem('token') });
          },
          periodos: function(rest){  
            return rest( 'evaluacion_periodo' ,'POST',{ token: localStorage.getItem('token') });
          },
          campos: function(rest){  
            return rest( 'campo' ,'POST',{ token: localStorage.getItem('token') });
          }
        }, 
        views: {
          '@': listTemplate
        }
            
      })

      .state(model+'.show',{ 
        url: '/{id:[0-9]{1,25}}', 
        resolve: {
          module: function(rest){  
            return rest( 'trabajador/vendedores' ,'POST',{ token: localStorage.getItem('token') });
          },
          periodos: function(rest){  
            return rest( 'evaluacion_periodo' ,'POST',{ token: localStorage.getItem('token') });
          },
          campos: function(rest){  
            return rest( 'campo' ,'POST',{ token: localStorage.getItem('token') });
          }
        }, 
        views: {
          '@': listTemplate,
          'sidenavRight@': modelShow
        }
      })

      .state( model +'.new',{
        url:'/new', 
        resolve: { 
          module: function(rest){  
            return rest( model ,'POST',{ token: localStorage.getItem('token') });
          }
        }, 
        views: {
          '@': listTemplate ,
          'sidenavRight@': modelNew
        }   
      }) ;

  });

function evaluacionListController ($scope, $rootScope, $state, module, periodos, campos, list,rest, validForm) {
  console.log(module, campos );
  $rootScope.app.user = module.respond.usuario.respond[0];
  $rootScope.app.menu = module.respond.asignacion_menu.respond;
  $rootScope.$emit('changeTitleToolbar', 'appbar',' Evaluación');
  //$rootScope.$emit('changeTypeToolbar','appbar', 'extend');
  $rootScope.$emit('hideAction',{ icon: 'mdfi_content_add', type: 'float' });

  var data = [ 
    { id_trabajador : 'id' }, 
    { apellido_paterno : 'title' }, 
    { nombres : 'subtitle' }, 
    { photo : 'photo' , value : function (value) { return  value.split('.')[0]+"-1x1."+value.split('.')[1]  }  }
  ] ;

  $scope.trabajadores = list(module.respond.trabajador.respond, data);
  $scope.periodos = periodos.respond.evaluacion_periodo.respond;

  $scope.periodoRegitered = false;
  $scope.periodoMessage = 'Pendiente';
  //$scope.campos = campos.respond.campo.respond;

  $scope.evaluar= function( id, event){
    $state.go('^.show', { id:id });
  };

  $scope.formApp= {
    campoevaluacion: {}
  };

  // Date
  var date = new Date( Date.now() );
  $scope.month= monthInString(date.getMonth());
  $scope.year= date.getFullYear();

  $scope.date = date.getDate()+'/'+(date.getMonth() + 1)+'/'+date.getFullYear();

  angular.forEach($scope.periodos, function(value, index){
    console.log(value,date.getMonth() + 1 , $scope.year);
    if ( value.anio == $scope.year && value.mes == (date.getMonth() + 1) ) {
      $scope.periodoRegitered = true;
      //$scope.periodoMessage = 'Periodo Registrado';
    };
  });

  $scope.formActions = {
    submit: function(form){
      var validFormResult = validForm(form) ;  

      if ( validFormResult.status ) {
        var dataInput = {token: localStorage.getItem('token') , evaluacion_campo: $scope.formApp.campoevaluacion }
        rest('evaluacion_campo/new','POST', dataInput).then(function(respond){  
            console.log(respond);
            if (respond.status) { 
              
              //$state.go('home');

            }else{

              $rootScope.$emit('showSnackbar', respond.message ) ;

            };

        });

      }else{
                   
        $rootScope.$emit('showSnackbar', validFormResult.message, form ) ;

      }; 
    }
  }

  $scope.tabEvaluar = true;

};

function evaluacionNewController ( $rootScope, $state ) { 

  $rootScope.$emit('changeTitleToolbar', 'toolbar-right','Registrar Item');
  $rootScope.app.toolbarIcon.cancel.show = true;
  $rootScope.app.toolbarIcon.cancel.click = function() {
    $state.go('^.list');
    $rootScope.$emit('toggleSidenav', 'right', false);
  };
  $rootScope.app.toolbarIcon.edit.show = false;
  $rootScope.app.toolbarIcon.delete.show = false;

  $rootScope.$emit('toggleSidenav', 'right', true);

  $rootScope.$on('clickOverlay', function(){
    $state.go('^.list');
  });

};
function evaluacionShowController ( $rootScope, $scope, $state, $verge, $stateParams, findByAttribute, module, campos, periodos) { 

  $rootScope.$emit('changeTitleToolbar', 'toolbar-right','Evaluar Vendedor');


  $rootScope.app.toolbarIcon.cancel.show = true;
  $rootScope.app.toolbarIcon.cancel.click = function() {
    $state.go('^.list');
    $rootScope.$emit('toggleSidenav', 'right', false);
  };
  $rootScope.app.toolbarIcon.edit.show = false;
  $rootScope.app.toolbarIcon.delete.show = false;

  $rootScope.$emit('toggleSidenav', 'right', true);

  $rootScope.$on('clickOverlay', function(){
    $state.go('^.list');
  });

  if ( $('.sidenav--right').find('.sidenav__content').height() > $verge.viewportH() ) {
    $('.sidenav--right').css('overflow-y', 'scroll');
  };

  /************************/

  $scope.item = findByAttribute( module.respond.trabajador.respond, 'id_trabajador', $stateParams.id )[0];
  $scope.periodos = periodos.respond.evaluacion_periodo.respond;


  angular.forEach($scope.periodos, function(value, index){
    //console.log(value,date.getMonth() + 1 , $scope.year);
    if ( value.anio == $scope.year && value.mes == (date.getMonth() + 1) ) {
      $scope.periodoRegitered = true;
      //$scope.periodoMessage = 'Periodo Registrado';
    };
  });

  var photo = $rootScope.serverUrl+'/public/persona/'+ $scope.item.photo ;
  $('#vendedor-photo').css( 'background-image', "url('"+ photo +"')" );

  $scope.formApp= {
    id_trabajador: $stateParams.id
  };

  $scope.formActions = {
    submit: function(form){
      var validFormResult = validForm(form) ;  

      if ( validFormResult.status ) {
        var dataInput = {token: localStorage.getItem('token') , evaluacion_campo: $scope.formApp }
        rest('evaluacion_campo/new/trabajador','POST', dataInput).then(function(respond){  
            console.log(respond);
            if (respond.status) { 
              
              //$state.go('home');

            }else{

              $rootScope.$emit('showSnackbar', respond.message ) ;

            };

        });

      }else{
                   
        $rootScope.$emit('showSnackbar', validFormResult.message, form ) ;

      }; 
    }
  }

};

function monthInString(month) {
  if (month === 9) {
    return 'Octubre';
  }else if (month === 10) {
    return 'Noviembre';
  }else if(month === 11){
    return 'Diciembre';
  };
}