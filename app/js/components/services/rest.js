	"use strict";
  
	angular.module('app')
    .factory('rest', function($http, $rootScope){
      return function(uri, method, data){ 
        return $http({
                      method:method, 
                      url: $rootScope.serverUrl+'/rest/'+ uri,  
                      data: $.param(data), 
                      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
              }).then(function(respond){ 
                  return respond.data ; 
        });
      };
    });
