  "use strict";
  
  angular.module('app')
    .factory('list', function(){

      function parseObject( row, data  ) {

        var item = {};

              angular.forEach( row, function(valueField,field){

                angular.forEach(data, function(dataRow, dataIndex){

                  if ( dataRow.hasOwnProperty(field) ) { 

                    if ( dataRow.hasOwnProperty('value') ) {
                      this[ dataRow[field] ] = dataRow.value(valueField) ;
                    }else{
                      this[ dataRow[field] ] = valueField ;
                    };
                    
                  };
                }, item);

              });

        return item;

      }

      return function ( list, data ) {
    
        var items = []; 

        if( angular.isArray(list) )  {

            angular.forEach( list , function(value,index){ //cross the array

              this[index]= parseObject( value, data  );

            }, items); 

            return items;

        }else if ( angular.isObject(list)  ) {
          return parseObject( list, data  );
        };

        return items;

      };

    });

