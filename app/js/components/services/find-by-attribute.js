'use strict';

angular.module('app')

    .factory('findByAttribute', function(){

    	function findByAttribute ( array, attribute, idValue ) {
	      
    		var result = [];
		    for (var i = 0; i < array.length; i++) {

		      angular.forEach( array[i] , function( value, field){

		        if ( field == attribute && value == idValue ) { result.push( array[i] ) };

		      });

		    };
		    return result;

	  	};

      return findByAttribute ;
    });