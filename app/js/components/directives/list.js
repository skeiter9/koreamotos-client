'use strict'; 

  angular.module('app')

    .directive('list',list); 

  function list ($rootScope){

    return {
      scope: {
        items: '=',
        type: '@', //multiline, inline(default)
        photo: '@',
        click: '='
      },   
      templateUrl: 'js/components/views/list.html',
      controller: function ($scope, $element, $attrs,$rootScope){
        //console.log($scope.click);
        var vm = this;

        vm.inline = ($scope.type === 'inline') ? true : false;
        vm.multiline = ($scope.type === 'multiline') ? true : false;
        vm.photo = (typeof($scope.photo)!=='undefined') ? true : false;
        vm.icon = ( vm.photo ) ? false : true;

        $scope.serverUrl = $rootScope.serverUrl;

        vm.nameIcon = function(name){
          console.log(name);
          return name.substring(0,1) || '' ;
        }

        // Order
        $scope.order=function(field){
          $scope.orderBy= field;
        }

      },
      controllerAs:'vm',
      compile: function(){
        return {
          pre: function preLink(scope, element, iAttrs, vm) {

            if ( scope.items.length === 0 ) {
              element.html(angular.element("<article class='card'><div class='card__content'> No hay registros </div></article> "));
            };  

          }, 
          post: function postLink(scope, element, iAttrs, vm) {

            
          }
        };
      }
    };
  }; 


     

