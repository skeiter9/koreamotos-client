'use strict';

angular.module('app',['yemd','ui.router']);

angular.module('app')
	.config(['$yemdProvider', '$stateProvider', '$urlRouterProvider', function($yemdProvider, $stateProvider, $urlRouterProvider) {

		$urlRouterProvider 
	    .rule(function ($injector, $location) {
	      var path = $location.path(),
	          normalized = path.toLowerCase();
	      if (path !== normalized) {
	        return normalized;
	      }
	  })
	  .otherwise('/');

	  $stateProvider
			.state('404',{
	      url:'/404',
	      templateUrl:'js/components/views/404.html',
	      controller: ['$scope','$rootScope', '$state',function($scope,$rootScope, $state){ 
	        var vm = this; 
	        $rootScope.$emit('changeTitleAppbar', '404 Error' );
	        $rootScope.$emit('changeAppbar', 'default' );
	      }],
	      controllerAs:'vm'
	  	});

	}])

	.run(['$state', '$urlRouter', '$rootScope', '$rootElement', 'rest', '$yemd', function($state, $urlRouter,$rootScope,$rootElement, rest, $yemd){

		$rootScope.app= {
	    logout: function(){
	      rest('usuario/logout', 'GET',{}).then(function(respond){  
	        if ( respond.status ) {  
	          $rootScope.$emit('showSnackbar', "Hasta Luego..." ) ;
	          $rootScope.app.user={};
	          return $state.go('login');
	        }else{
	          $rootScope.$emit('showSnackbar', "No puedo cerrar sesión: "+respond.message) ;
	        }  
	      });
	    }
	  };

	  //Check Auth
		$rootScope.$on('$locationChangeSuccess', function(evt) { 

	    evt.preventDefault();  

	    rest('usuario/login','GET',{}).then(function(query){ 

	      if ( query.status ) {

	        $rootScope.app.user  = { status: true, data: query.respond.data , menu: query.respond.menu } ; 

	        if ( $state.current.name.indexOf('.list') !== -1 || $state.current.name.indexOf('.new') !== -1 ) {
            $rootScope.$emit('changeAppbar', 'extend');
            $rootScope.$emit('changeTitleAppbar', $state.current.name.split('.')[0] );
	        } else {
	        	$rootScope.$emit('hideAction');
            $rootScope.$emit('changeAppbar', 'default');
            $rootScope.$emit('changeTitleAppbar', 'Korea Motos' );
	        }

	        if ( $state.current.name.indexOf('.new') !== -1 ) {
	        	$rootScope.$on('clickOverlay', function(e){
	        		$state.go('^.list');
	        	});
	        };

	        ($state.current.name==='login')? $state.go('home') : $urlRouter.sync();

	      } else {  

	        if ($state.current.name!=='login') $state.go('login');

	      }

	    }); 

	  });

		//Not found
		$rootScope.$on('$stateNotFound', function(event, unfoundState, fromState, fromParams){
	    console.log("state ",unfoundState.to," not found");
	    //console.log(unfoundState.to); // "lazy.state"
	    //console.log(unfoundState.toParams); // {a:1, b:2}
	    //console.log(unfoundState.options); // {inherit:false} + default options
	    event.preventDefault();
	    $state.go('404',{stateFail:unfoundState.to}); //redirect 404 error
	  });

	}]);

'use strict';

angular.module('app')
	.controller('mainCtrl', ['$rootScope', '$rootElement', '$state', '$urlRouter', 'rest', function($rootScope, $rootElement, $state, $urlRouter, rest){

		console.log($rootScope.app);
		
	}]);
'use strict';

angular.module('app')
	.controller('menu', ['$scope', '$state', '$rootScope', function($scope, $state, $rootScope){

		$scope.goToState = function( state ) {
			$state.go(state);
			$rootScope.$emit('toggleSidenav', 'left', false);
		}

	}]);

'use strict';

angular.module('app',['yemd','ui.router']);

angular.module('app')
	.config(['$yemdProvider', '$stateProvider', '$urlRouterProvider', function($yemdProvider, $stateProvider, $urlRouterProvider) {

		$urlRouterProvider 
	    .rule(function ($injector, $location) {
	      var path = $location.path(),
	          normalized = path.toLowerCase();
	      if (path !== normalized) {
	        return normalized;
	      }
	  })
	  .otherwise('/');

	  $stateProvider
			.state('404',{
	      url:'/404',
	      templateUrl:'js/components/views/404.html',
	      controller: ['$scope','$rootScope', '$state',function($scope,$rootScope, $state){ 
	        var vm = this; 
	        $rootScope.$emit('changeTitleAppbar', '404 Error' );
	        $rootScope.$emit('changeAppbar', 'default' );
	      }],
	      controllerAs:'vm'
	  	});

	}])

	.run(['$state', '$urlRouter', '$rootScope', '$rootElement', 'rest', '$yemd', function($state, $urlRouter,$rootScope,$rootElement, rest, $yemd){

		$rootScope.app= {
	    logout: function(){
	      rest('usuario/logout', 'GET',{}).then(function(respond){  
	        if ( respond.status ) {  
	          $rootScope.$emit('showSnackbar', "Hasta Luego..." ) ;
	          $rootScope.app.user={};
	          return $state.go('login');
	        }else{
	          $rootScope.$emit('showSnackbar', "No puedo cerrar sesión: "+respond.message) ;
	        }  
	      });
	    }
	  };

	  //Check Auth
		$rootScope.$on('$locationChangeSuccess', function(evt) { 

	    evt.preventDefault();  

	    rest('usuario/login','GET',{}).then(function(query){ 

	      if ( query.status ) {

	        $rootScope.app.user  = { status: true, data: query.respond.data , menu: query.respond.menu } ; 

	        if ( $state.current.name.indexOf('.list') !== -1 || $state.current.name.indexOf('.new') !== -1 ) {
            $rootScope.$emit('changeAppbar', 'extend');
            $rootScope.$emit('changeTitleAppbar', $state.current.name.split('.')[0] );
	        } else {
	        	$rootScope.$emit('hideAction');
            $rootScope.$emit('changeAppbar', 'default');
            $rootScope.$emit('changeTitleAppbar', 'Korea Motos' );
	        }

	        if ( $state.current.name.indexOf('.new') !== -1 ) {
	        	$rootScope.$on('clickOverlay', function(e){
	        		$state.go('^.list');
	        	});
	        };

	        ($state.current.name==='login')? $state.go('home') : $urlRouter.sync();

	      } else {  

	        if ($state.current.name!=='login') $state.go('login');

	      }

	    }); 

	  });

		//Not found
		$rootScope.$on('$stateNotFound', function(event, unfoundState, fromState, fromParams){
	    console.log("state ",unfoundState.to," not found");
	    //console.log(unfoundState.to); // "lazy.state"
	    //console.log(unfoundState.toParams); // {a:1, b:2}
	    //console.log(unfoundState.options); // {inherit:false} + default options
	    event.preventDefault();
	    $state.go('404',{stateFail:unfoundState.to}); //redirect 404 error
	  });

	}]);

'use strict';

angular.module('app')
	.controller('mainCtrl', ['$rootScope', '$rootElement', '$state', '$urlRouter', 'rest', function($rootScope, $rootElement, $state, $urlRouter, rest){

		console.log($rootScope.app);
		
	}]);
'use strict';

angular.module('app')
	.controller('menu', ['$scope', '$state', '$rootScope', function($scope, $state, $rootScope){

		$scope.goToState = function( state ) {
			$state.go(state);
			$rootScope.$emit('toggleSidenav', 'left', false);
		}

	}]);

'use strict'; 

	angular.module('app')
		.directive('yemdForm',form);

	function form($rootScope, $compile, validForm, rest, $state){
		return {
			scope: {
				inputs:'=',
				models: '=',
				module: '=',
				type  : '@',
				iditem: '='
			},  
			restrict:'A', 
			require:'form',
			controller:  ['$scope', '$element', '$attrs', '$rootScope', '$compile', 'validForm', function($scope,$element,$attrs,$rootScope,$compile,validForm ){ 
				console.log('form');
				$scope.submit={};

				if ($scope.type==='update') $scope.submit.value='Actualizar' ;

				if ($scope.type==='new')    $scope.submit.value='Registrar' ;

				if ( typeof($scope.type)==='undefined' ) $scope.submit.value='Registrar' //default

				if (!$scope.inputs.status){
					$scope.template= $scope.inputs.message ;
				}else {

					var template='';
					$scope.options={};
					angular.forEach($scope.inputs.respond, function(value,index){
						switch (value.type){
							case 'textarea':
								var required = (value.required)? 'required' : '' ;
								template += "<textarea name='"+value.name+"' "+required+" placeholder='"+value.name+"' ng-model='models."+value.name+"'></textarea>";
							break;
							case 'switch': 
								template +="<input type='checkbox' ng-true-value='1' ng-false-value='0' placeholder='"+value.name+"' name='"+value.name+"' ng-model='models."+value.name+"'>";
							break;
							case 'select': 
								var required = (value.required)? 'required' : '' ;
								template += "<select data-yemd-selecto name='"+value.name+"' yemd-select  "+required+" placeholder='"+value.name+"' ng-model='models."+value.name+"' ng-options='model."+value.name+" as model.nombres for model in options."+value.name+"' data-second-action='mdfi_content_add' ><option value=''>Seleccione "+value.name.substr(3)+"</option></select> ";
								$scope.options[value.name] = value.options;
							break;
							default:
								//$scope.models[value.name] = ($scope.type==='update')? value.value : '' ;
								var required = (value.required)? 'required' : '' ;  /*max='"+value.max+"'*/
								template += "<input type='"+value.type+"' name='"+value.name+"' "+required+"  placeholder='"+value.name+"' ng-model='models."+value.name+"'/>";
							break;
						}
					}); 

					template += "<input type='submit' value='"+ $scope.submit.value +"' ng-model='models.submit' ng-click='submit()' />";
				} 

				$scope.template= angular.element( template ); 
 
				// fill modes if type === update
				if ($scope.type==='update'){
					angular.forEach($scope.inputs.respond, function(value,index){
						if ( value.type==='number' ) {
							console.log(value.value);
							$scope.models[value.name]= parseFloat(value.value) ;
						} else{
							$scope.models[value.name]=value.value;
						};
						
					});
					console.log($scope.models);
				} 

			}],
			compile: function(){
				return {
	        pre: function preLink(scope, element, attrs, require) {  

	        	element.append( scope.template );

						var elementI = $compile(scope.template)(scope);
 						
						var titleAppbar= (scope.type==='update')? 'Editar '+scope.module :'Nuevo '+scope.module;

            
						$rootScope.$emit('changeIcon',{oldAction:'sidenavLeft', newAction:'back', newIcon:'arrow-left'});
	        },  
	        post: function postLink(scope, element, attrs, require) {   

	        	scope.submit  = function(){ 

	        		if ( scope.type==='new' ) {
	        			var validFormResult = validForm(require) ; 
					 			if ( validFormResult.status ) {
					 				rest( scope.module+'/new' ,'POST', scope.models).then(function(respond){  
		                console.log(respond);
		                if (respond.status) { 
		                	$rootScope.$emit('toggleSidenav', 'right', false) ;
		                  $state.go('^.list');
		                }else{ 
		                  $rootScope.$emit('showSnackbar', respond.message ) ;
		                };
		              });
					 			}else{
					 				console.log("display the error messages",validFormResult);
					 			}; 

	        		}else if(scope.type==='update'){
	        			console.log(scope.module+'/'+scope.iditem);
	        			rest( scope.module+'/'+scope.iditem ,'PUT', scope.models).then(function(respond){  
		              console.log(respond);
		              if (respond.status) { 
		              	$rootScope.$emit('toggleSidenav', 'right', false) ;
		                $state.go('^.list');
		              }else{ 
		                $rootScope.$emit('showSnackbar', respond.message ) ;
		              };
		            });
	        		}

			         
				 		}

				  }
	      };
			}
		};
	}
	form.$inject = ['$rootScope', '$compile', 'validForm', 'rest', '$state'];;

	


(function(yemd){  
  'use strict'; 

  function list($rootScope,$compile,$rootElement,$filter,$state,$document){
    return {
      scope: {
        items: '=',
        module: '='
      }, 
      restrict:'E',  
      controller: ['$scope', '$element', '$attrs', '$rootScope', '$compile', '$rootElement', '$filter', '$state', function  ($scope, $element, $attrs,$rootScope,$compile,$rootElement,$filter,$state){
        var vm =this ; 

        vm.field=function(field){
          if ( field.indexOf('_')!==-1  ) { return field.replace(/_/g,' ')}
          else if( field.toLowerCase()==='area' ){ return 'Área'}
          else if( field.toLowerCase()==='descripcion' ){ return 'descripción'}
          else{ return field }; 
        };

        vm.value=function(field,value){
          if ( field==='precio_puesto_en_planta'  ) { return $filter('currency')(value, 'S/. '); } 
          else{ return value }; 
        };

        $scope.title = $state.current.name.split('.')[0];  
        //$scope.title = $scope.title[0]; 

        angular.forEach($scope.items, function(value,index){
          angular.forEach(value, function(valueField,field){
            if ( field.indexOf('id_')!== -1 && field !== 'id_'+$scope.module ) {  
              delete $scope.items[index][field] ;
            };
            if (field === 'id_'+$scope.module) { delete $scope.items[index][field] ; $scope.items[index].id= valueField ; };
            if (field.indexOf('estado')!== -1) { $scope.items[index][field] = (valueField==='1')?'Activo':'No ctivo'; };
          });
        }); 
        
        $scope.search=''; 
        
        $scope.selectItem= function(){ 
          $rootScope.$emit('removeFormSearch');
          $rootScope.$emit('removeIconSearch');
        }
        // Order
        $scope.order=function(field){
          $scope.orderBy= field;
        }
      }],
      controllerAs:'vm',
      templateUrl: 'app/components/views/list.html',
      compile: function(){
        return {
          pre: function preLink(scope, element, iAttrs, vm) {
            if ( scope.items.length === 0 ) {
              element.append(angular.element("<h2 class='sub-title'> No hay registros de "+scope.title+" </h2> "));
            };  
            //element.addClass( scope.className );
            $rootScope.$emit('changeAppbar', 'extend' );
            $rootScope.$emit('showAction','embed'); 
            //$rootScope.$emit('createFormSearch');
            $rootScope.$emit('changeTitleAppbar', scope.title );  
          }, 
          post: function postLink(scope, element, iAttrs, vm) {
            $rootScope.$on('cleanFormSearch',function(e){
              scope.search=''; 
            });  
            
          }
        };
      }
    };
  }
  list.$inject = ['$rootScope', '$compile', '$rootElement', '$filter', '$state', '$document'];; 


  yemd.directive('list',list);    

})(yemd);
 

(function(angular,yemd){

	function form(){
		this.template=function(query){
			if (!query.status) return query ;
			var template='';
			angular.forEach(query.respond, function(value,index){
				switch (value.type){
					case 'number':
						var required = (value.required)? 'required' : '' ;
						template    += "<input type='"+value.type+"' name='"+value.name+"' "+required+" max='"+value.max+"' placeholder='"+value.name+"'/>";
					break;
					default:
						template +="<input/>";
					break;
				}
			}, template);
			return template;

		};
		return this.template;
	}

	yemd.factory('formHtml',form);
})(angular,yemd);

'use strict';

angular.module('app').factory('listHtml', [ function(){
    return function(query,select ){
      var result= {};
      var newQuery= [];
      if ( angular.isArray(query) ) {
        angular.forEach(query, function(valueI,indexI){ // travel of all rows
          var row={};
          angular.forEach(valueI, function(value,key){ // individual row and retrieve field,key
            //angular.forEach(value, function(contentOneRow,fieldOneRow){ //key and field of individual row
              angular.forEach(select, function(content,index){ //select array
                if ( angular.isObject(content) ) {
                  angular.forEach(content, function(nameView,field){
                    if ( key === field && typeof(content.filter)==='undefined' ) { this[nameView]=value };
                    if ( key === field && typeof(content.filter)!=='undefined') { this[nameView]= content.filter(value) };
                  }, row);
                }else if (  key === content ) { 
                  this[key]= value ;
                };
              }, row);
            //});
          });
          this.push(row);
        },newQuery);
        return newQuery;
      }else{
        angular.forEach(query, function(value,key){ 
          angular.forEach(select, function(content,index){
            if ( angular.isObject(content) ) {
              angular.forEach(content, function(nameView,field){
                if ( key === field && typeof(content.filter)==='undefined' ) { this[nameView]=value };
                if ( key === field && typeof(content.filter)!=='undefined')  { this[nameView]= content.filter(value) };
              }, result);
            }else if (  key === content ) { 
              this[key]=value ;
            };
          }, result);
        });
        return result;
      }; 
    };  
  }]);
  "use strict";
  
  angular.module('app')
    .factory('list', function(){

      return function ( list, data ) {
    
          var items = [],
              item = {};

          angular.forEach( list , function(value,index){

              angular.forEach(value, function(valueField,field){

                angular.forEach(data, function(dataRow, dataIndex){

                  if ( dataRow.hasOwnProperty(field) ) { 

                    if ( dataRow.hasOwnProperty('value') ) {
                      this[ dataRow[field] ] = dataRow.value(valueField) ;
                    }else{
                      this[ dataRow[field] ] = valueField ;
                    };
                    
                  };
                }, item);

              });

              this[index]= item;

          }, items); 

        return items;

      };

    });


	"use strict";
  
	angular.module('app')
    .factory('rest', ['$http', function($http){
      return function(uri, method, data){ 
        return $http({
                      method:method, 
                      url: 'http://koreamotors.com.pe/rest/'+uri,  
                      data: $.param(data), 
                      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
              }).then(function(respond){ 
                  return respond.data ; 
        });
      };
    }]);

(function(yemd){  
  
  'use strict'; 

  function showItem($rootScope,$compile,$rootElement,$state,$stateParams){
    return {
      scope: {
        item: '=', 
        module: '=',
        title: '='
      }, 
      restrict:'E',  
      controller: ['$scope', '$element', '$attrs', '$rootScope', '$compile', '$rootElement', '$state', '$stateParams', function  ($scope, $element, $attrs,$rootScope,$compile,$rootElement,$state,$stateParams){
        var vm =this ;
            vm.item = {};

        angular.forEach($scope.item, function(valueField,field){
            
            //if ( field.indexOf('id_')!== -1 ) {   delete $scope.item[field] ; }; 

            //if ( field.indexOf('estado')!== -1) {  
              //$scope.item[field] = (valueField==='1')?'Activo':'No ctivo'; 
            //};

            if ( field.indexOf('precio_puesto_en_planta') !== -1) {  
              vm.item['Precio puesto en planta'] = valueField; 
            }else if( field.indexOf('id_') === -1 ){
              vm.item[field]= valueField;
            };

        });

      }],
      controllerAs:'vm',
      templateUrl: 'app/components/views/show.html',
      compile: function(){
        return { 
          pre: function preLink(scope, element, iAttrs, vm) {  
            //$rootScope.$emit('changeIcon',{oldAction:'sidenavLeft', newAction:'back', newIcon:'arrow-left'}); 
            //$rootScope.$emit('changeAppbar', 'default' );
            //$rootScope.$emit('hideActionNew'); 
            //$rootScope.$emit('changeTitleAppbar', scope.module.toUpperCase()+': '+scope.title.toUpperCase() ); 
            //$rootScope.$emit('createIconEdit');   
          }, 
          post: function postLink(scope, element, iAttrs, vm) {
            
          }
        };
      }
    };
  }
  showItem.$inject = ['$rootScope', '$compile', '$rootElement', '$state', '$stateParams'];; 


  yemd.directive('showItem',showItem);   

})(yemd);
 

'use strict';

angular.module('app')

  .config( ['$stateProvider', function( $stateProvider ) {

  	$stateProvider

  	  .state('home',{
          url:'/home',
          resolve: { 
            usuario: ['rest', function(rest){  
              return rest('usuario/login','GET',{});
            }]
          },
          templateUrl:'js/home/home.html',
          controller: ['$rootScope', '$scope', 'usuario', function($rootScope,$scope, usuario){

            $rootScope.$emit('changeAppbar', 'default' );
            $rootScope.$emit('changeTitleAppbar', 'Home' ); 
            var vm =this;  
            vm.usuario   = (usuario.status)?usuario.respond.data:{}; 

          }],
          controllerAs:'vm'
      });

  }]);

'use strict';

angular.module('app')

.config(['$stateProvider' ,function( $stateProvider ) {

  $stateProvider

  .state('login',{
        url:'/',
        templateUrl:'js/login/login.html',
        controller: ['$scope', '$rootScope', 'validForm', 'rest', '$state', function($scope,$rootScope,validForm,rest,$state){ 
          var vm = this;

          $rootScope.$emit('changeAppbar', 'default' );
          $rootScope.$emit('changeTitleAppbar', 'Login' ); 

          $scope.app={ };  

          vm.login = function(form){  
            var validFormResult = validForm(form) ;  
            console.log( validFormResult );
            if (validFormResult.status) {  
              rest('usuario/login','POST', $scope.app).then(function(respond){  
                console.log(respond);
                if (respond.status) { 
                  $state.go('home');
                }else{ 
                  $rootScope.$emit('showSnackbar', respond.message ) ;
                };
              });
            }else{
              $rootScope.$emit('showSnackbar', validFormResult.message ) ;
            };  
          };

        }],
        controllerAs:'vm'
  });


}]);

'use strict';

angular.module('app')
  .config(['$stateProvider', function( $stateProvider ) {

    var model = 'trabajador',
        modelUri = 'vendedores',
        listTemplate = {
          templateUrl: "js/vendedores/vendedores-list.html" ,
          controller: function($scope, $rootScope, $yemd, $state, module, list){ 

            var data = [ 
                          { apellido_paterno : 'apellido paterno' }, 
                          { apellido_materno : 'apellido materno' }, 
                          { nombres : 'nombres' }, 
                          { dni : 'dni' }, 
                          { area : 'área' }, 
                          { cargo : 'cargo' }, 
                          { estado : 'estado del trabajador' , value : function (value) { return ( value==='1') ? 'Activo' : 'No ctivo'  }  } 
            ] ;

            $scope.items = list(module.respond, data);

            $rootScope.$emit('showAction',{ icon: 'mdfi_content_add', type: 'embed', nodeClose: '.appbar', classSpecial: 'action--in-appbar' });
            $rootScope.$on('clickAction',function(e){ $state.go('^.new');  });
          }
        };


	  $stateProvider

      .state(model,{
        abstract: true, 
        url:'/'+modelUri
      })  

      .state(model+'.list',{
        url:'',
        resolve: { 
          module: ['rest', function(rest){  
            return rest( model ,'GET',{});
          }]
        }, 
        views: {
          '@': listTemplate
        }
            
      })

      .state( model +'.new',{
        url:'/new', 
        resolve: { 
          module:['rest',function(rest){  
            return rest( model,'GET',{});
          }],
          form:['rest',function(rest){  
            return rest(model + '/new','GET',{});
          }]
        }, 
        views: {
          '@': listTemplate ,
          'sidenavRight@': {
            templateUrl: "js/vendedores/vendedores-new.html" ,
            controller: ['$scope', '$rootScope', '$yemd', 'form', 'rest', function($scope, $rootScope, $yemd,  form, rest ){  
              console.log(form);
              $scope.module= model ; 
              $scope.data=form;
              $scope.app={};  
              $scope.aux={};  

              $rootScope.$emit('toggleSidenav', 'right', true);
              
              $rootScope.$emit('specialWidthSidenav', 'right', 'x2');

              $rootScope.$on('secondActionNew', function(e, field){
                var table = field.split('_')[1];

                var html = "<header class='modal__title'>Registrar nuevo "+ table +"</header>"+
                           "<section class='modal__content'> <p> Form new  </p> </section>"+
                           "<section class='modal__controls'> <a class='modal__controls__option'>Agregar</a> <a class='modal__controls__option'>Cancelar</a>  </section>";
                
                $rootScope.$emit('toggleModal', true, html);

              });

            }]
          }
        }   
      }) ;

  }]);

'use strict'; 

	angular.module('app')
		.directive('yemdForm',form);

	function form($rootScope, $compile, validForm, rest, $state){
		return {
			scope: {
				inputs:'=',
				models: '=',
				module: '=',
				type  : '@',
				iditem: '='
			},  
			restrict:'A', 
			require:'form',
			controller:  ['$scope', '$element', '$attrs', '$rootScope', '$compile', 'validForm', function($scope,$element,$attrs,$rootScope,$compile,validForm ){ 
				console.log('form');
				$scope.submit={};

				if ($scope.type==='update') $scope.submit.value='Actualizar' ;

				if ($scope.type==='new')    $scope.submit.value='Registrar' ;

				if ( typeof($scope.type)==='undefined' ) $scope.submit.value='Registrar' //default

				if (!$scope.inputs.status){
					$scope.template= $scope.inputs.message ;
				}else {

					var template='';
					$scope.options={};
					angular.forEach($scope.inputs.respond, function(value,index){
						switch (value.type){
							case 'textarea':
								var required = (value.required)? 'required' : '' ;
								template += "<textarea name='"+value.name+"' "+required+" placeholder='"+value.name+"' ng-model='models."+value.name+"'></textarea>";
							break;
							case 'switch': 
								template +="<input type='checkbox' ng-true-value='1' ng-false-value='0' placeholder='"+value.name+"' name='"+value.name+"' ng-model='models."+value.name+"'>";
							break;
							case 'select': 
								var required = (value.required)? 'required' : '' ;
								template += "<select data-yemd-selecto name='"+value.name+"' yemd-select  "+required+" placeholder='"+value.name+"' ng-model='models."+value.name+"' ng-options='model."+value.name+" as model.nombres for model in options."+value.name+"' data-second-action='mdfi_content_add' ><option value=''>Seleccione "+value.name.substr(3)+"</option></select> ";
								$scope.options[value.name] = value.options;
							break;
							default:
								//$scope.models[value.name] = ($scope.type==='update')? value.value : '' ;
								var required = (value.required)? 'required' : '' ;  /*max='"+value.max+"'*/
								template += "<input type='"+value.type+"' name='"+value.name+"' "+required+"  placeholder='"+value.name+"' ng-model='models."+value.name+"'/>";
							break;
						}
					}); 

					template += "<input type='submit' value='"+ $scope.submit.value +"' ng-model='models.submit' ng-click='submit()' />";
				} 

				$scope.template= angular.element( template ); 
 
				// fill modes if type === update
				if ($scope.type==='update'){
					angular.forEach($scope.inputs.respond, function(value,index){
						if ( value.type==='number' ) {
							console.log(value.value);
							$scope.models[value.name]= parseFloat(value.value) ;
						} else{
							$scope.models[value.name]=value.value;
						};
						
					});
					console.log($scope.models);
				} 

			}],
			compile: function(){
				return {
	        pre: function preLink(scope, element, attrs, require) {  

	        	element.append( scope.template );

						var elementI = $compile(scope.template)(scope);
 						
						var titleAppbar= (scope.type==='update')? 'Editar '+scope.module :'Nuevo '+scope.module;

            
						$rootScope.$emit('changeIcon',{oldAction:'sidenavLeft', newAction:'back', newIcon:'arrow-left'});
	        },  
	        post: function postLink(scope, element, attrs, require) {   

	        	scope.submit  = function(){ 

	        		if ( scope.type==='new' ) {
	        			var validFormResult = validForm(require) ; 
					 			if ( validFormResult.status ) {
					 				rest( scope.module+'/new' ,'POST', scope.models).then(function(respond){  
		                console.log(respond);
		                if (respond.status) { 
		                	$rootScope.$emit('toggleSidenav', 'right', false) ;
		                  $state.go('^.list');
		                }else{ 
		                  $rootScope.$emit('showSnackbar', respond.message ) ;
		                };
		              });
					 			}else{
					 				console.log("display the error messages",validFormResult);
					 			}; 

	        		}else if(scope.type==='update'){
	        			console.log(scope.module+'/'+scope.iditem);
	        			rest( scope.module+'/'+scope.iditem ,'PUT', scope.models).then(function(respond){  
		              console.log(respond);
		              if (respond.status) { 
		              	$rootScope.$emit('toggleSidenav', 'right', false) ;
		                $state.go('^.list');
		              }else{ 
		                $rootScope.$emit('showSnackbar', respond.message ) ;
		              };
		            });
	        		}

			         
				 		}

				  }
	      };
			}
		};
	};

	


(function(yemd){  
  'use strict'; 

  function list($rootScope,$compile,$rootElement,$filter,$state,$document){
    return {
      scope: {
        items: '=',
        module: '='
      }, 
      restrict:'E',  
      controller: ['$scope', '$element', '$attrs', '$rootScope', '$compile', '$rootElement', '$filter', '$state', function  ($scope, $element, $attrs,$rootScope,$compile,$rootElement,$filter,$state){
        var vm =this ; 

        vm.field=function(field){
          if ( field.indexOf('_')!==-1  ) { return field.replace(/_/g,' ')}
          else if( field.toLowerCase()==='area' ){ return 'Área'}
          else if( field.toLowerCase()==='descripcion' ){ return 'descripción'}
          else{ return field }; 
        };

        vm.value=function(field,value){
          if ( field==='precio_puesto_en_planta'  ) { return $filter('currency')(value, 'S/. '); } 
          else{ return value }; 
        };

        $scope.title = $state.current.name.split('.')[0];  
        //$scope.title = $scope.title[0]; 

        angular.forEach($scope.items, function(value,index){
          angular.forEach(value, function(valueField,field){
            if ( field.indexOf('id_')!== -1 && field !== 'id_'+$scope.module ) {  
              delete $scope.items[index][field] ;
            };
            if (field === 'id_'+$scope.module) { delete $scope.items[index][field] ; $scope.items[index].id= valueField ; };
            if (field.indexOf('estado')!== -1) { $scope.items[index][field] = (valueField==='1')?'Activo':'No ctivo'; };
          });
        }); 
        
        $scope.search=''; 
        
        $scope.selectItem= function(){ 
          $rootScope.$emit('removeFormSearch');
          $rootScope.$emit('removeIconSearch');
        }
        // Order
        $scope.order=function(field){
          $scope.orderBy= field;
        }
      }],
      controllerAs:'vm',
      templateUrl: 'app/components/views/list.html',
      compile: function(){
        return {
          pre: function preLink(scope, element, iAttrs, vm) {
            if ( scope.items.length === 0 ) {
              element.append(angular.element("<h2 class='sub-title'> No hay registros de "+scope.title+" </h2> "));
            };  
            //element.addClass( scope.className );
            $rootScope.$emit('changeAppbar', 'extend' );
            $rootScope.$emit('showAction','embed'); 
            //$rootScope.$emit('createFormSearch');
            $rootScope.$emit('changeTitleAppbar', scope.title );  
          }, 
          post: function postLink(scope, element, iAttrs, vm) {
            $rootScope.$on('cleanFormSearch',function(e){
              scope.search=''; 
            });  
            
          }
        };
      }
    };
  }
  list.$inject = ['$rootScope', '$compile', '$rootElement', '$filter', '$state', '$document'];; 


  yemd.directive('list',list);    

})(yemd);
 

(function(angular,yemd){

	function form(){
		this.template=function(query){
			if (!query.status) return query ;
			var template='';
			angular.forEach(query.respond, function(value,index){
				switch (value.type){
					case 'number':
						var required = (value.required)? 'required' : '' ;
						template    += "<input type='"+value.type+"' name='"+value.name+"' "+required+" max='"+value.max+"' placeholder='"+value.name+"'/>";
					break;
					default:
						template +="<input/>";
					break;
				}
			}, template);
			return template;

		};
		return this.template;
	}

	yemd.factory('formHtml',form);
})(angular,yemd);

'use strict';

angular.module('app').factory('listHtml', [ function(){
    return function(query,select ){
      var result= {};
      var newQuery= [];
      if ( angular.isArray(query) ) {
        angular.forEach(query, function(valueI,indexI){ // travel of all rows
          var row={};
          angular.forEach(valueI, function(value,key){ // individual row and retrieve field,key
            //angular.forEach(value, function(contentOneRow,fieldOneRow){ //key and field of individual row
              angular.forEach(select, function(content,index){ //select array
                if ( angular.isObject(content) ) {
                  angular.forEach(content, function(nameView,field){
                    if ( key === field && typeof(content.filter)==='undefined' ) { this[nameView]=value };
                    if ( key === field && typeof(content.filter)!=='undefined') { this[nameView]= content.filter(value) };
                  }, row);
                }else if (  key === content ) { 
                  this[key]= value ;
                };
              }, row);
            //});
          });
          this.push(row);
        },newQuery);
        return newQuery;
      }else{
        angular.forEach(query, function(value,key){ 
          angular.forEach(select, function(content,index){
            if ( angular.isObject(content) ) {
              angular.forEach(content, function(nameView,field){
                if ( key === field && typeof(content.filter)==='undefined' ) { this[nameView]=value };
                if ( key === field && typeof(content.filter)!=='undefined')  { this[nameView]= content.filter(value) };
              }, result);
            }else if (  key === content ) { 
              this[key]=value ;
            };
          }, result);
        });
        return result;
      }; 
    };  
  }]);
  "use strict";
  
  angular.module('app')
    .factory('list', function(){

      return function ( list, data ) {
    
          var items = [],
              item = {};

          angular.forEach( list , function(value,index){

              angular.forEach(value, function(valueField,field){

                angular.forEach(data, function(dataRow, dataIndex){

                  if ( dataRow.hasOwnProperty(field) ) { 

                    if ( dataRow.hasOwnProperty('value') ) {
                      this[ dataRow[field] ] = dataRow.value(valueField) ;
                    }else{
                      this[ dataRow[field] ] = valueField ;
                    };
                    
                  };
                }, item);

              });

              this[index]= item;

          }, items); 

        return items;

      };

    });


	"use strict";
  
	angular.module('app')
    .factory('rest', ['$http', function($http){
      return function(uri, method, data){ 
        return $http({
                      method:method, 
                      url: 'http://koreamotors.com.pe/rest/'+uri,  
                      data: $.param(data), 
                      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
              }).then(function(respond){ 
                  return respond.data ; 
        });
      };
    }]);

(function(yemd){  
  
  'use strict'; 

  function showItem($rootScope,$compile,$rootElement,$state,$stateParams){
    return {
      scope: {
        item: '=', 
        module: '=',
        title: '='
      }, 
      restrict:'E',  
      controller: ['$scope', '$element', '$attrs', '$rootScope', '$compile', '$rootElement', '$state', '$stateParams', function  ($scope, $element, $attrs,$rootScope,$compile,$rootElement,$state,$stateParams){
        var vm =this ;
            vm.item = {};

        angular.forEach($scope.item, function(valueField,field){
            
            //if ( field.indexOf('id_')!== -1 ) {   delete $scope.item[field] ; }; 

            //if ( field.indexOf('estado')!== -1) {  
              //$scope.item[field] = (valueField==='1')?'Activo':'No ctivo'; 
            //};

            if ( field.indexOf('precio_puesto_en_planta') !== -1) {  
              vm.item['Precio puesto en planta'] = valueField; 
            }else if( field.indexOf('id_') === -1 ){
              vm.item[field]= valueField;
            };

        });

      }],
      controllerAs:'vm',
      templateUrl: 'app/components/views/show.html',
      compile: function(){
        return { 
          pre: function preLink(scope, element, iAttrs, vm) {  
            //$rootScope.$emit('changeIcon',{oldAction:'sidenavLeft', newAction:'back', newIcon:'arrow-left'}); 
            //$rootScope.$emit('changeAppbar', 'default' );
            //$rootScope.$emit('hideActionNew'); 
            //$rootScope.$emit('changeTitleAppbar', scope.module.toUpperCase()+': '+scope.title.toUpperCase() ); 
            //$rootScope.$emit('createIconEdit');   
          }, 
          post: function postLink(scope, element, iAttrs, vm) {
            
          }
        };
      }
    };
  }
  showItem.$inject = ['$rootScope', '$compile', '$rootElement', '$state', '$stateParams'];; 


  yemd.directive('showItem',showItem);   

})(yemd);
 

'use strict';

angular.module('app')

  .config( ['$stateProvider', function( $stateProvider ) {

  	$stateProvider

  	  .state('home',{
          url:'/home',
          resolve: { 
            usuario: ['rest', function(rest){  
              return rest('usuario/login','GET',{});
            }]
          },
          templateUrl:'js/home/home.html',
          controller: ['$rootScope', '$scope', 'usuario', function($rootScope,$scope, usuario){

            $rootScope.$emit('changeAppbar', 'default' );
            $rootScope.$emit('changeTitleAppbar', 'Home' ); 
            var vm =this;  
            vm.usuario   = (usuario.status)?usuario.respond.data:{}; 

          }],
          controllerAs:'vm'
      });

  }]);

'use strict';

angular.module('app')

.config(['$stateProvider' ,function( $stateProvider ) {

  $stateProvider

  .state('login',{
        url:'/',
        templateUrl:'js/login/login.html',
        controller: ['$scope', '$rootScope', 'validForm', 'rest', '$state', function($scope,$rootScope,validForm,rest,$state){ 
          var vm = this;

          $rootScope.$emit('changeAppbar', 'default' );
          $rootScope.$emit('changeTitleAppbar', 'Login' ); 

          $scope.app={ };  

          vm.login = function(form){  
            var validFormResult = validForm(form) ;  
            console.log( validFormResult );
            if (validFormResult.status) {  
              rest('usuario/login','POST', $scope.app).then(function(respond){  
                console.log(respond);
                if (respond.status) { 
                  $state.go('home');
                }else{ 
                  $rootScope.$emit('showSnackbar', respond.message ) ;
                };
              });
            }else{
              $rootScope.$emit('showSnackbar', validFormResult.message ) ;
            };  
          };

        }],
        controllerAs:'vm'
  });


}]);

'use strict';

angular.module('app')
  .config(['$stateProvider', function( $stateProvider ) {

    var model = 'trabajador',
        modelUri = 'vendedores',
        listTemplate = {
          templateUrl: "js/vendedores/vendedores-list.html" ,
          controller: function($scope, $rootScope, $yemd, $state, module, list){ 

            var data = [ 
                          { apellido_paterno : 'apellido paterno' }, 
                          { apellido_materno : 'apellido materno' }, 
                          { nombres : 'nombres' }, 
                          { dni : 'dni' }, 
                          { area : 'área' }, 
                          { cargo : 'cargo' }, 
                          { estado : 'estado del trabajador' , value : function (value) { return ( value==='1') ? 'Activo' : 'No ctivo'  }  } 
            ] ;

            $scope.items = list(module.respond, data);

            $rootScope.$emit('showAction',{ icon: 'mdfi_content_add', type: 'embed', nodeClose: '.appbar', classSpecial: 'action--in-appbar' });
            $rootScope.$on('clickAction',function(e){ $state.go('^.new');  });
          }
        };


	  $stateProvider

      .state(model,{
        abstract: true, 
        url:'/'+modelUri
      })  

      .state(model+'.list',{
        url:'',
        resolve: { 
          module: ['rest', function(rest){  
            return rest( model ,'GET',{});
          }]
        }, 
        views: {
          '@': listTemplate
        }
            
      })

      .state( model +'.new',{
        url:'/new', 
        resolve: { 
          module:['rest',function(rest){  
            return rest( model,'GET',{});
          }],
          form:['rest',function(rest){  
            return rest(model + '/new','GET',{});
          }]
        }, 
        views: {
          '@': listTemplate ,
          'sidenavRight@': {
            templateUrl: "js/vendedores/vendedores-new.html" ,
            controller: ['$scope', '$rootScope', '$yemd', 'form', 'rest', function($scope, $rootScope, $yemd,  form, rest ){  
              console.log(form);
              $scope.module= model ; 
              $scope.data=form;
              $scope.app={};  
              $scope.aux={};  

              $rootScope.$emit('toggleSidenav', 'right', true);
              
              $rootScope.$emit('specialWidthSidenav', 'right', 'x2');

              $rootScope.$on('secondActionNew', function(e, field){
                var table = field.split('_')[1];

                var html = "<header class='modal__title'>Registrar nuevo "+ table +"</header>"+
                           "<section class='modal__content'> <p> Form new  </p> </section>"+
                           "<section class='modal__controls'> <a class='modal__controls__option'>Agregar</a> <a class='modal__controls__option'>Cancelar</a>  </section>";
                
                $rootScope.$emit('toggleModal', true, html);

              });

            }]
          }
        }   
      }) ;

  }]);
