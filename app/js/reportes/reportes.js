'use strict';

angular.module('app')
	
	.config(function( $stateProvider ) {

    var model = 'reporte',
        modelUri = 'reportes',
        listTemplate = {
          templateUrl: "js/"+modelUri+'/'+modelUri+"-list.html" ,
          controller: pedidosListController
        },
        modelNew = {
          templateUrl: "js/"+modelUri+'/'+modelUri+"-new.html" ,
          controller: pedidosNewController
        },
        modelShow = {
          templateUrl: "js/"+modelUri+'/'+modelUri+"-show.html",
          controller: pedidosShowController
        };

	  $stateProvider

      .state( model,{
          abstract: true, 
          url:'/'+modelUri
        })  

      .state(model+'.list',{
        url:'',
        resolve: { 
          module: function(rest){  
            return rest( model ,'POST',{ token: localStorage.getItem('token') });
          }
        }, 
        views: {
          '@': listTemplate
        }
            
      })

      .state(model+'.show',{ 
        url: '/{id:[0-9]{1,25}}', 
        resolve: {
          module: function(rest){  
            return rest( model ,'POST',{ token: localStorage.getItem('token') });
          }
        }, 
        views: {
          '@': listTemplate,
          'sidenavRight@': modelShow
        }
      })

      .state( model +'.new',{
        url:'/new', 
        resolve: { 
          module: function(rest){  
            return rest( model ,'POST',{ token: localStorage.getItem('token') });
          }
        }, 
        views: {
          '@': listTemplate ,
          'sidenavRight@': modelNew
        }   
      }) ;

  });

function pedidosListController ($scope) {

};

function pedidosNewController () {  };
function pedidosShowController () {  };