'use strict';

angular.module('app')
	
	.config(function( $stateProvider ) {

    var model = 'item',
        modelUri = 'items',
        listTemplate = {
          template: "<section data-list data-items='items' data-type='inline' ></section>" ,
          controller: itemListController
        },
        modelNew = {
          templateUrl: "js/"+modelUri+'/'+modelUri+"-new.html" ,
          controller: itemNewController
        },
        modelShow = {
          templateUrl: "js/"+modelUri+'/'+modelUri+"-show.html",
          controller: itemShowController
        };

	  $stateProvider

      .state( model,{
          abstract: true, 
          url:'/'+modelUri
        })  

      .state(model+'.list',{
        url:'',
        resolve: { 
          module: function(rest){  
            return rest( model ,'POST',{ token: localStorage.getItem('token') });
          }
        }, 
        views: {
          '@': listTemplate
        }
            
      })

      .state(model+'.show',{ 
        url: '/{id:[0-9]{1,25}}', 
        resolve: {
          module: function(rest){  
            return rest( model ,'POST',{ token: localStorage.getItem('token') });
          }
        }, 
        views: {
          '@': listTemplate,
          'sidenavRight@': modelShow
        }
      })

      .state( model +'.new',{
        url:'/new', 
        resolve: { 
          module: function(rest){  
            return rest( model ,'POST',{ token: localStorage.getItem('token') });
          }
        }, 
        views: {
          '@': listTemplate ,
          'sidenavRight@': modelNew
        }   
      }) ;

  });

function itemListController ($scope, $rootScope, $state, module, list) {
  console.log(module);
  $rootScope.app.user = module.respond.usuario.respond[0];
  $rootScope.app.menu = module.respond.asignacion_menu.respond;
  $rootScope.$emit('changeTitleToolbar', 'appbar','Items de Evaluación');
  $rootScope.$emit('showAction',{ icon: 'mdfi_content_add', type: 'float' });
  $rootScope.$emit('changeTypeToolbar','appbar', 'normal');

  $rootScope.$on('clickAction',function(e){ $state.go('^.new');  });

  var data = [ 
    { id_item : 'id' }, 
    { nombre : 'title' }, 
    { descripcion : 'subtitle' }
  ] ;

  $scope.items = list(module.respond.item.respond, data);
  console.log($scope.items);
};

function itemNewController ( $rootScope, $state ) { 

  $rootScope.$emit('changeTitleToolbar', 'toolbar-right','Registrar Item');
  $rootScope.app.toolbarIcon.cancel.show = true;
  $rootScope.app.toolbarIcon.cancel.click = function() {
    $state.go('^.list');
    $rootScope.$emit('toggleSidenav', 'right', false);
  };
  $rootScope.app.toolbarIcon.edit.show = false;
  $rootScope.app.toolbarIcon.delete.show = false;

  $rootScope.$emit('toggleSidenav', 'right', true);

  $rootScope.$on('clickOverlay', function(){
    $state.go('^.list');
  });

};
function itemShowController () {  };