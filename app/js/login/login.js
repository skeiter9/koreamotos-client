'use strict';

angular.module('app')

  .config(['$stateProvider' ,function( $stateProvider ) {

    $stateProvider

    .state('login',{
      url: '/',
      templateUrl: 'js/login/login.html',
      resolve: { 
        loggued: function(rest){  
          return rest('usuario/login','POST',{ token: localStorage.getItem('token') });
        }
      },
      controller: loginController
      } 
    );


  }]);


function loginController ( loggued, $scope,$rootScope,validForm,rest,$state ) {

  $rootScope.$emit('changeTypeToolbar', 'appbar','default');
  $rootScope.$emit('hideAction');
  $rootScope.$emit('changeTitleToolbar', 'appbar','Login');

  if (loggued.status) { return $state.go('home') } ;
  

   $scope.formApp={ };  
   $scope.formAppAux={ };  

    $scope.login = function(form){  

      var validFormResult = validForm(form) ;  

      if (validFormResult.status) {  

        rest('usuario/login','POST', $scope.formApp).then(function(respond){  
          console.log(respond);
          if (respond.status) { 
            
            localStorage.setItem('token', respond.token );
            $rootScope.app.logged = true;
            $state.go('home');

          }else{

            $rootScope.$emit('showSnackbar', respond.message ) ;

          };

        });

      }else{
        console.log(respond);
        $rootScope.$emit('showSnackbar', validFormResult.message ) ;

      };  

    };

}
