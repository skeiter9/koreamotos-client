module.exports = function(grunt){
	// config
	grunt.initConfig({
		watch:{ 
			scripts:{
        files:['app/*.html', 'app/scss/*.scss', 'app/js/*.js', 'app/js/**/*.js', 'app/js/**/*.html', 'app/css/*.css'],
        options: {
         	livereload: 1333
        }
      },
			sass:{
				files:['app/scss/*.scss', 'app/scss/components/*.scss'],
				tasks:['sass']
			}
		},
		connect: {
		  server: {
		    options: {
		    	port: 9003,
		    	base:'app/',
		     	livereload: 1333
		    }
		  }	
		},
	  sass: {
	      dist: {
	        files: {
	          'app/css/app.css':'app/scss/app.scss'
	        }
	      },
        options:{
          style:'compressed'
        }
	  },
	  ngAnnotate: {
      options: {
          singleQuotes: true
      },
      app: {
        files: {
          'app/js/app.full.js': ['app/js/app.js', 'app/js/app-controller.js', 'app/js/menu-controller.js', 'app/js/**/*.js']
        }
      }
    },
    uglify: {
	    app: {
	      files: {
	        'dist/js/app.min.js': ['dist/js/app.full.js']
	      }
	    }
	  }
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	
	grunt.loadNpmTasks('grunt-ng-annotate');

  grunt.registerTask('default',['connect', 'watch'] );
  grunt.registerTask('production',['ngAnnotate', 'uglify'] );

};
